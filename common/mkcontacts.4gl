IMPORT FGL libutil

FUNCTION create_database(tp,ws)
    DEFINE tp VARCHAR(20), ws BOOLEAN
    DEFINE tmp VARCHAR(200), s INTEGER

    WHENEVER ERROR CONTINUE
    DROP TABLE datafilter
    --
    DROP TABLE contnote
    DROP TABLE contact
    DROP TABLE city
    WHENEVER ERROR STOP

    IF tp=="cleanup" THEN
       RETURN
    END IF

    IF tp=="server" THEN

       LET s = libutil.sequence_drop("contact")
       --LET s = libutil.sequence_drop("contnote")

       CALL libutil.users_server_table()
       IF ws THEN
          CALL libutil.users_add(v_undef, NULL, v_undef_text) -- Foreign key to contacts
          CALL libutil.users_add("max",   NULL, "Max Brand")
          CALL libutil.users_add("mike",  NULL, "Mike Sharp")
          CALL libutil.users_add("ted",   NULL, "Ted Philips")
       END IF

       CALL libutil.datafilter_table()
       IF ws THEN
          -- Data filters for user "mike"
          LET tmp = " city_num = 1000 OR city_country IN ('France','Germany')"
          CALL libutil.datafilter_define("mike", "city", tmp)
          CALL libutil.datafilter_define("mike", "contact", "contact_city IN (select city_num FROM city WHERE"||tmp||")")
          -- Data filters for user "max"
          LET tmp = " city_num = 1000 OR city_country IN ('France','Spain')"
          CALL libutil.datafilter_define("max", "city", tmp)
          CALL libutil.datafilter_define("max", "contact", "contact_city IN (select city_num FROM city WHERE"||tmp||")")
          -- Data filters for user "ted"
          CALL libutil.datafilter_define("ted", "city", NULL)
          CALL libutil.datafilter_define("ted", "contact", NULL)
       END IF

    END IF

    -- Readonly table for the mobile, filled with city of the user area (some countries)
    -- Filtered in the mobile application program, according to datafilter.
    CREATE TABLE city (
         city_num INTEGER NOT NULL,
         city_name VARCHAR(30) NOT NULL,
         city_country VARCHAR(30) NOT NULL,
         PRIMARY KEY(city_num),
         UNIQUE (city_name, city_country)
    )

   INSERT INTO city VALUES (   1,"METROPOLITANA",          "METROPOLITANA" )
   INSERT INTO city VALUES (   2,"CENTRAL",                "GUATEMALA" )
   INSERT INTO city VALUES (   3,"REGION 3 ESCUINTLA",     "REGION 3 ESCUINTLA" )
   INSERT INTO city VALUES (   4,"REGION 4 JUTIAPA",       "REGION 4 JUTIAPA" )
   INSERT INTO city VALUES ( 401,"REGION 401",             "HUEHUETENANGO" )
   INSERT INTO city VALUES ( 402,"REGION 402",             "LA DEMOCRACIA" )
   INSERT INTO city VALUES (   5,"REGION 5 JALAPA",        "REGION 5 JALAPA" )
   INSERT INTO city VALUES (   6,"REGION 6",               "CHIQUIMULA" )
   INSERT INTO city VALUES (   7,"REGION 7",               "COMITE ZACAPA" )
   INSERT INTO city VALUES (   8,"REGION 8",               "IZABAL" )
   INSERT INTO city VALUES (   9,"REGION 9",               "SANTA ELENA" )
   INSERT INTO city VALUES (  10,"REGION 10",              "ALTA VERAPAZ" )
   INSERT INTO city VALUES (  11,"REGION 11",              "BAJA VERAPAZ" )
   INSERT INTO city VALUES (  12,"REGION 12",              "QUICHE" )
   INSERT INTO city VALUES (  13,"REGION 13",              "HUEHUETENANGO-" )
   INSERT INTO city VALUES (  14,"REGION 14",              "DEMOCRACIA" )
   INSERT INTO city VALUES (  17,"ATITLAN",                "ATITLAN" )
   INSERT INTO city VALUES (  18,"REGION 18",              "COMITE OCCIDENTE" )
   INSERT INTO city VALUES (  19,"REGION 19 METROPOLITANA","REGION 19 METROPOLITANA" )
   INSERT INTO city VALUES (  20,"REGION 20 METROPOLITANA","REGION 20 METROPOLITANA" )
   INSERT INTO city VALUES (  22,"REGION 22 METROPOLITANA","REGION 22 METROPOLITANA" )
   INSERT INTO city VALUES (  23,"REGION 23 METROPOLITANA","REGION 23 METROPOLITANA" )
   INSERT INTO city VALUES (  24,"REGION 24 METROPOLITANA","REGION 24 METROPOLITANA" )
   INSERT INTO city VALUES (  25,"REGION 25 METROPOLITANA","REGION 25 METROPOLITANA" )
   INSERT INTO city VALUES (  27,"REGION 27 METROPOLITANA","REGION 27 METROPOLITANA" )
   INSERT INTO city VALUES (  28,"REGION 28 METROPOLITANA","REGION 28 METROPOLITANA" )
   INSERT INTO city VALUES (  29,"REGION 29 METROPOLITANA","REGION 29 METROPOLITANA" )
   INSERT INTO city VALUES (  30,"REGION 30",              "SAN LUIS" )
   INSERT INTO city VALUES (  31,"REGION 31",              "JOYABAJ" )
   INSERT INTO city VALUES (  32,"REGION 32 SANTA ROSA",   "REGION 32 SANTA ROSA" )
   INSERT INTO city VALUES (  33,"REGION 33 COATEPEQUE",   "REGION 33 COATEPEQUE" )
   INSERT INTO city VALUES (  35,"REGION 35 ANTIGUA",      "REGION 35 ANTIGUA" )
   INSERT INTO city VALUES (  41,"REGION 41",              "EL PROGRESO" )
   INSERT INTO city VALUES (  42,"REGION 42",              "LA LIBERTAD" )
   INSERT INTO city VALUES (  43,"REGION 43 SANTA LUCIA",  "REGION 43 SANTA LUCIA" )
   INSERT INTO city VALUES (  44,"REGION 44",              "ESQUIPULAS" )
   INSERT INTO city VALUES (  46,"REGION 46 CUILAPA",      "REGION 46 CUILAPA" )
   INSERT INTO city VALUES (1000,"<undef>",                "<undef>" )
    
    {INSERT INTO city VALUES ( 900, "Xela", "Guatemala" )
    INSERT INTO city VALUES ( 1000, v_undef, v_undef_text )
    INSERT INTO city VALUES ( 1001, "Paris", "France" )
    INSERT INTO city VALUES ( 1002, "London", "U.K." )
    INSERT INTO city VALUES ( 1003, "Berlin", "Germany" )
    INSERT INTO city VALUES ( 1004, "Madrid", "Spain" )
    INSERT INTO city VALUES ( 1005, "Rome", "Italy" )
    INSERT INTO city VALUES ( 1006, "Vienna", "Austria" )
    INSERT INTO city VALUES ( 1007, "Schiltigheim", "France" )
    INSERT INTO city VALUES ( 1008, "Vendenheim", "France" )
    INSERT INTO city VALUES ( 1009, "Bishheim", "France" )
    INSERT INTO city VALUES ( 1010, "Strasbourg", "France" )}

    CREATE TABLE contact (
         contact_num INTEGER NOT NULL,
         contact_rec_muser VARCHAR(20) NOT NULL,
         contact_rec_mtime DATETIME YEAR TO FRACTION(3) NOT NULL,
         contact_rec_mstat CHAR(2) NOT NULL,
         contact_name VARCHAR(100) NOT NULL,
         contact_valid CHAR(1) NOT NULL,
         contact_street VARCHAR(240),
         contact_city INTEGER NOT NULL,
         contact_num_m VARCHAR(40),
         contact_num_w VARCHAR(40),
         contact_num_h VARCHAR(40),
         contact_user VARCHAR(20) NOT NULL,
         contact_loc_lon DECIMAL(10,6),
         contact_loc_lat DECIMAL(10,6),
         contact_photo_mtime DATETIME YEAR TO FRACTION(3),
         contact_photo BYTE,

         contact_cod_region   INTEGER,

         contact_estado_caso char(18),
         contact_operacion char(20) not null ,
         contact_producto varchar(18),
         contact_garantia varchar(40),
         contact_estado_op varchar(20),
         contact_dpi_cliente varchar(20),
         contact_tel_cliente varchar(240),
         contact_tel_casa varchar(10),
         contact_tel_cel varchar(10),
         contact_tel_trab varchar(10),
         contact_tel_otro varchar(10),
         contact_saldo_cap varchar(20),
         contact_cap_vencido varchar(20),
         contact_int_vencido varchar(20),
         contact_saldo_imo varchar(20),
         contact_otros varchar(20),
         contact_total varchar(20),
         contact_porc_serv varchar(20),
         contact_total_cobrar varchar(20),
         contact_fec_ult_pago varchar(20),
         contact_cuotas_mora varchar(20),
         contact_fec_mora varchar(20),
         contact_nom_fia1 varchar(240),
         contact_dir_fia1 varchar(240),
         contact_nom_fia2 varchar(240),
         contact_dir_fia2 varchar(240),
         contact_nom_fia3 varchar(240),
         contact_dir_fia3 varchar(240),
         contact_when datetime year to fraction(3),
         
         PRIMARY KEY(contact_num),
         UNIQUE (contact_name, contact_city), -- contact_street) for unique tests
         FOREIGN KEY (contact_city) REFERENCES city (city_num),
         FOREIGN KEY (contact_user) REFERENCES users (user_id)
    )
    IF NOT ws THEN
       LET s = libutil.sequence_create("contact",1000)
    ELSE
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when )
         VALUES ( 1001, "admin", "2010-01-01 00:00:00.000", "S",
            "Billy Jean",      "Y", "6 Rue de Kléber",       900, "03-7645-2345",
            NULL, NULL, "max", NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
      { INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1002, "admin", "2010-01-01 00:00:00.000", "S",
            "Michael Jackson", "Y", "5 Rue Voltaire",  1008, "03-1111-2345",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1003, "admin", "2010-01-01 00:00:00.000", "S",
            "Ted Turner",     "Y", "Rue du Canal",          1009, "03-9999-1111",
            NULL, NULL, "mike", NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1004, "admin", "2010-01-01 00:00:00.000", "S",
            "Ted Turner",    "Y", "5 Place Kléber",        1010, "03-9999-2345",
            NULL, NULL, "ted", NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1005, "admin", "2010-01-01 00:00:00.000", "S",
            "Clark Brinship", "Y", "2 Rue Rouge",           1007, "03-9999-2345",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1006, "admin", "2010-01-01 00:00:00.000", "S",
            "Mike Clamberg",  "Y", "3 Rue des Artisans",    1008, "03-7645-9999",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1007, "admin", "2010-01-01 00:00:00.000", "S",
            "Ted Fiztman",    "Y", "123 Ocean Av",          1002, "03-7645-9999",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1008, "admin", "2010-01-01 00:00:00.000", "S",
            "Patrick Kenzal", "Y", "8722 Main street",      1004, "03-9999-2345",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1009, "admin", "2010-01-01 00:00:00.000", "S",
            "Steve Baumer",   "Y", "231 Cardigon Bld",      1002, "03-9999-2345",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
       INSERT INTO contact(contact_num, contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_name, contact_valid, contact_street, contact_city, contact_num_m,
            contact_num_w, contact_num_h, contact_user, contact_loc_lon, contact_loc_lat,
            contact_photo_mtime, contact_photo, contact_cod_region, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,     
            contact_dpi_cliente, contact_tel_cliente, contact_tel_casa, contact_tel_cel,       
            contact_tel_trab, contact_tel_otro, contact_saldo_cap, contact_cap_vencido,   
            contact_int_vencido, contact_saldo_imo, contact_otros, contact_total, 
            contact_porc_serv, contact_total_cobrar, contact_fec_ult_pago, contact_cuotas_mora,
            contact_fec_mora, contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, 
            contact_dir_fia2, contact_nom_fia3, contact_dir_fia3, contact_when) 
         VALUES ( 1010, "admin", "2010-01-01 00:00:00.000", "S",
            "Philip Desmond", "Y", "12 Kirt street",        1004, "03-7645-9999",
            NULL, NULL, v_undef, NULL, NULL, NULL, NULL, NULL, NULL,
            "1001", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)}
       LET s = libutil.sequence_create("contact",2000)
    END IF

    CREATE TABLE contnote (
         contnote_num            INTEGER NOT NULL,
         contnote_rec_muser      VARCHAR(20) NOT NULL,
         contnote_rec_mtime      DATETIME YEAR TO FRACTION(3) NOT NULL,
         contnote_rec_mstat      CHAR(2) NOT NULL,
         contnote_contact        INTEGER NOT NULL,
         contnote_when           DATETIME YEAR TO FRACTION(3) NOT NULL,
         contnote_text           VARCHAR(250),
         contnote_cod_tipologia  CHAR(2),
         contnote_fecha_promesa  DATE,
         contnote_monto_promesa  DECIMAL(15,5),
         contnote_boleta_numero  VARCHAR(25),
         contnote_boleta_fecha   DATE,
         contnote_boleta_monto   DECIMAL(15,5),
         contnote_cuenta         VARCHAR(25),
         contnote_boleta_copia   CHAR(1),
         contnote_justificacion  VARCHAR(200),
         PRIMARY KEY(contnote_num),
         UNIQUE (contnote_contact, contnote_when),
         FOREIGN KEY (contnote_contact) REFERENCES contact (contact_num)
    )
    IF NOT ws THEN
       LET s = libutil.sequence_create("contnote",1000)
    ELSE
       INSERT INTO contnote VALUES ( 1001, "admin", "2010-01-01 00:00:00.000", "S",
              1001, "2014-01-01 11:45:00.000", "This customer is French", "49",
              NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL )
       INSERT INTO contnote VALUES ( 1002, "admin", "2010-01-01 00:00:00.000", "S",
              1001, "2014-01-02 12:43:00.000", "Send a gift for Xmass", "49",
              NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL )
       LET s = libutil.sequence_create("contnote",50000)
    END IF

    CREATE TABLE tipologia (
      cod_tipologia  CHAR(2) NOT NULL ,
      des_tipologia  VARCHAR(60),
      estado         CHAR(1),
      orden          SMALLINT
    )

   INSERT INTO tipologia VALUES ( "18","Cliente ya Pago","V",0)
   INSERT INTO tipologia VALUES ( "66","Aplicacion de pago parcial","A",1)
   INSERT INTO tipologia VALUES ( "86","Pago Parcial","A",2)
   INSERT INTO tipologia VALUES ( "9","Promesa de Pago","V",3)
   INSERT INTO tipologia VALUES ( "49","Cliente Prometio Pagar","A",4)
   INSERT INTO tipologia VALUES ( "C1","Confirmar Promesa de Pago","A",5)
   INSERT INTO tipologia VALUES ( "F3","Seguimiento de Promesa de Pago","A",6)
   INSERT INTO tipologia VALUES ( "21","Confirmar Pago","A",7)
   INSERT INTO tipologia VALUES ( "80","Casos Especiales","V",8)
   INSERT INTO tipologia VALUES ( "E8","Cesion Voluntaria del Inmueble","A",9)
   INSERT INTO tipologia VALUES ( "22","Cliente solicita entregar garantia","A",10)
   INSERT INTO tipologia VALUES ( "46","Cliente dice que nunca solicito el credito","A",11)
   INSERT INTO tipologia VALUES ( "47","Inconforme con el saldo","A",12)
   INSERT INTO tipologia VALUES ( "D8","Convenio de Pago","A",13)
   INSERT INTO tipologia VALUES ( "E5","Debito a Cuenta","A",14)
   INSERT INTO tipologia VALUES ( "57","Pago / Reestructuracion","A",15)
   INSERT INTO tipologia VALUES ( "14","Arreglo de Pago","A",16)
   INSERT INTO tipologia VALUES ( "25","Cliente espera reestructuracion","A",17)
   INSERT INTO tipologia VALUES ( "51","Reestructuracion","A",18)
   INSERT INTO tipologia VALUES ( "23","Tramite Pendiente","V",19)
   INSERT INTO tipologia VALUES ( "55","Cliente Envia Carta","A",20)
   INSERT INTO tipologia VALUES ( "C5","En negociacion","A",21)
   INSERT INTO tipologia VALUES ( "72","Se presento a Cita y negocio","A",22)
   INSERT INTO tipologia VALUES ( "C6","Mayor Presion y Estrategia  de Cobro","A",23)
   INSERT INTO tipologia VALUES ( "90","Se Programo Reunion","A",24)
   INSERT INTO tipologia VALUES ( "15","Visita Cliente","V",25)
   INSERT INTO tipologia VALUES ( "C4","Visita-Investigacion de Deudor","A",26)
   INSERT INTO tipologia VALUES ( "37","Cliente recibio la Nota","A",28)
   INSERT INTO tipologia VALUES ( "83","Se envio Notificacion de Cobro","A",29)
   INSERT INTO tipologia VALUES ( "44","Se envio Carta/Telegrama","A",30)
   INSERT INTO tipologia VALUES ( "D1","Carta1","A",31)
   INSERT INTO tipologia VALUES ( "D2","Carta2","A",32)
   INSERT INTO tipologia VALUES ( "D3","Carta3","A",33)
   INSERT INTO tipologia VALUES ( "D4","Carta4","A",34)
   INSERT INTO tipologia VALUES ( "71","Se presento a Cita y no negocio","A",36)
   INSERT INTO tipologia VALUES ( "38","Cliente no recibio la Nota","A",37)
   INSERT INTO tipologia VALUES ( "69","Cliente No se presento a la cita","A",38)
   INSERT INTO tipologia VALUES ( "73","Resultado de visita a cliente","V",39)
   INSERT INTO tipologia VALUES ( "88","Personal Administrativo Ministerio de Educacion","A",40)
   INSERT INTO tipologia VALUES ( "34","Nueva Llamada","V",41)
   INSERT INTO tipologia VALUES ( "84","Resultado llamada desde Bufete","A",42)
   INSERT INTO tipologia VALUES ( "F1","Se llamo a Codeudor/Fiador","A",43)
   INSERT INTO tipologia VALUES ( "63","Cliente Devuelve Llamada","A",44)
   INSERT INTO tipologia VALUES ( "41","Se llamo a Referencias","V",45)
   INSERT INTO tipologia VALUES ( "45","Cliente cuelga el telefono","A",46)
   INSERT INTO tipologia VALUES ( "4","No contestan - Ocupado","V",47)
   INSERT INTO tipologia VALUES ( "5","Telefono no le pertenece","A",48)
   INSERT INTO tipologia VALUES ( "3","Linea Da�ada","A",49)
   INSERT INTO tipologia VALUES ( "2","Telefono Equivocado","A",50)
   INSERT INTO tipologia VALUES ( "42","No hay telefono Contacto","A",51)
   INSERT INTO tipologia VALUES ( "67","Se envio mensaje de texto","A",52)
   INSERT INTO tipologia VALUES ( "12","Mensaje Casa","V",53)
   INSERT INTO tipologia VALUES ( "13","Mensaje Celular","V",54)
   INSERT INTO tipologia VALUES ( "16","Mensaje Oficina","V",55)
   INSERT INTO tipologia VALUES ( "40","Se dejo mensaje con Fiador/Codeudor","V",56)
   INSERT INTO tipologia VALUES ( "C3","Mensaje Casa u Oficina y/o Referencias/ Fiadores","A",57)
   INSERT INTO tipologia VALUES ( "F4","Credito en Investigacion","V",58)
   INSERT INTO tipologia VALUES ( "93","Actualizacion de Informacion de cliente","A",59)
   INSERT INTO tipologia VALUES ( "68","Solicitud de Referencias","A",60)
   INSERT INTO tipologia VALUES ( "6","Sin capacidad de pago","V",61)
   INSERT INTO tipologia VALUES ( "7","Motivos Economicos","A",62)
   INSERT INTO tipologia VALUES ( "D5","No se le notifico al cliente","A",63)
   INSERT INTO tipologia VALUES ( "26","Cliente esta de viaje","A",64)
   INSERT INTO tipologia VALUES ( "27","Cliente esta enfermo","A",65)
   INSERT INTO tipologia VALUES ( "28","Cliente esta sin trabajo","A",66)
   INSERT INTO tipologia VALUES ( "30","Cliente emigro","A",67)
   INSERT INTO tipologia VALUES ( "10","Cliente Incontactable","A",68)
   INSERT INTO tipologia VALUES ( "11","Cliente Ilocalizable","V",69)
   INSERT INTO tipologia VALUES ( "33","Analisis  traslado a cobro judicial","A",70)
   INSERT INTO tipologia VALUES ( "87","Traslado a otra instancia","A",71)
   INSERT INTO tipologia VALUES ( "F2","Credito Trasladado a Cobro Judicial","A",72)
   INSERT INTO tipologia VALUES ( "F6","Trasladado a Demanda","A",73)
   INSERT INTO tipologia VALUES ( "61","Prestamo en Cobro Judicial","A",74)
   INSERT INTO tipologia VALUES ( "62","Proceso de Cobro Judicial","A",75)
   INSERT INTO tipologia VALUES ( "31","Cliente fallecido","V",76)
   INSERT INTO tipologia VALUES ( "52","Fallecimiento","A",77)
   INSERT INTO tipologia VALUES ( "1","Recoger cheque","A",78)
   INSERT INTO tipologia VALUES ( "8","Mal Servicio","A",79)
   INSERT INTO tipologia VALUES ( "17","Actualizacion de Cuenta para Cobro","A",80)
   INSERT INTO tipologia VALUES ( "19","Investigacion Declinada","A",81)
   INSERT INTO tipologia VALUES ( "24","Cliente vendra al Banco","V",82)
   INSERT INTO tipologia VALUES ( "20","Paga por Descuento","A",83)
   INSERT INTO tipologia VALUES ( "32","Cuenta bloqueada","A",84)
   INSERT INTO tipologia VALUES ( "35","Reprogramar a otro gestor","A",85)
   INSERT INTO tipologia VALUES ( "36","Cheque Rechazado","A",86)
   INSERT INTO tipologia VALUES ( "39","Solicitar Expediente","V",87)
   INSERT INTO tipologia VALUES ( "43","Tratamientos","A",88)
   INSERT INTO tipologia VALUES ( "48","Llamada de Agencia","A",89)
   INSERT INTO tipologia VALUES ( "50","Expediente entregado","A",90)
   INSERT INTO tipologia VALUES ( "53","Reestructuracion declinada","A",91)
   INSERT INTO tipologia VALUES ( "54","Cheque Pre-fechado","A",92)
   INSERT INTO tipologia VALUES ( "56","Cheque Prefechado","A",93)
   INSERT INTO tipologia VALUES ( "58","Tramite / Bloqueo","A",94)
   INSERT INTO tipologia VALUES ( "59","Se envio E-mail","V",95)
   INSERT INTO tipologia VALUES ( "60","Reclamo de Seguro ","A",96)
   INSERT INTO tipologia VALUES ( "64","Atendido en Area de Servcio al Cliente","A",97)
   INSERT INTO tipologia VALUES ( "65","Tramite en Asegurdora","A",98)
   INSERT INTO tipologia VALUES ( "74","Desbloqueo de prestamo","A",99)
   INSERT INTO tipologia VALUES ( "76","Renovacion Sobregiro","A",100)
   INSERT INTO tipologia VALUES ( "78","Gestion Realizada por Cobro Externo","A",101)
   INSERT INTO tipologia VALUES ( "82","Pago de convenio por acreditamiento","A",102)
   INSERT INTO tipologia VALUES ( "85","Atendido en Area de Servicio al Cliente","A",103)
   INSERT INTO tipologia VALUES ( "89","Gestion Serprojusa","A",104)
   INSERT INTO tipologia VALUES ( "91","Consulta de Datos Externa","A",105)
   INSERT INTO tipologia VALUES ( "92","Gestion de Serintegro","A",106)
   INSERT INTO tipologia VALUES ( "94","Gestion Secogua","A",107)
   INSERT INTO tipologia VALUES ( "95","Proceso de Liquidacion de Fideicomiso","A",108)
   INSERT INTO tipologia VALUES ( "96","Gestion Bufete Chavarria Milian","A",109)
   INSERT INTO tipologia VALUES ( "97","Gestion Tetel","A",110)
   INSERT INTO tipologia VALUES ( "98","Gestion TecSerFin","A",111)
   INSERT INTO tipologia VALUES ( "99","Gestion Global Leasing (SIC)","A",112)
   INSERT INTO tipologia VALUES ( "C2","Renovacion/Cheque Prefechado/Tramite/Reestructura","A",113)
   INSERT INTO tipologia VALUES ( "C7","Verificacion de Expediente","A",114)
   INSERT INTO tipologia VALUES ( "D6","Estado de Cuenta","A",116)
   INSERT INTO tipologia VALUES ( "D7","Resumen de Gestiones-Cobro Externo","A",117)
   INSERT INTO tipologia VALUES ( "D9","Credito sin Seguro","A",118)
   INSERT INTO tipologia VALUES ( "E1","Gestion Gericsa","A",119)
   INSERT INTO tipologia VALUES ( "E3","Trasladado a Cobro Externo","A",122)
   INSERT INTO tipologia VALUES ( "E4","Credito Bloqueado","A",123)
   INSERT INTO tipologia VALUES ( "F5","Analisis de Gestion de Cobro","A",124)
   INSERT INTO tipologia VALUES ( "E6","Cobro Pre Mora","A",125)
   INSERT INTO tipologia VALUES ( "E7","Cuenta Reasignada","A",126)
   INSERT INTO tipologia VALUES ( "E9","Aplicacion de Saldo a Favor","A",127)   

END FUNCTION
